<?php

namespace App\Http\Controllers;
use App\Product;

use Illuminate\Http\Request;

class ProductController extends Controller
{
	public function index()
	{
		return Product::all();
	}

	public function show(Product $product)
	{
		return $product;
	}

	public function store(Request $request)
	{
		$data = json_decode($request->json()->all());
		$product = Product::create($data);

		return response()->json($product, 201);
	}

	public function update(Request $request, Product $product)
	{
		$data = json_decode($request->json()->all());
		$product->update($data);

		return response()->json($product, 200);
	}

	public function delete(Product $product)
	{
		$product->delete();

		return response()->json(null, 204);
	}
}
