<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;

class CheckapiController extends Controller
{
	public function getData($id=null)
	{
		$response = Curl::to('http://localhost/mobpair/api/products/'.$id)
		->get();
		dd($response);
	}

	public function storeData(){
		$jsonData = ['product_name'=>'Computers', 'product_discription'=>'This is for test', 'sku'=>'CFV533'];
		$response = Curl::to('http://localhost/mobpair/api/products')
		->withData(json_encode($jsonData))
		->post();

		dd($response);
	}

	public function updateData($id)
	{
		$jsonData = ['product_name'=>'Computers', 'product_discription'=>'This is for test', 'sku'=>'CFV533'];
		
		$response = Curl::to('http://localhost/mobpair/api/products/'.$id)
		->withData(json_encode($jsonData))
		->put();

		dd($response);

	}

	public function deleteData($id)
	{

		$response = Curl::to('http://localhost/mobpair/api/products/'.$id)->delete();
		dd($response);

	}
}
