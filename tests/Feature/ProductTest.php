<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testsProductCreatedCorrectly()
    {     
        $payload = [
            'product_name' => 'Computer',
            'product_discription' => 'This is For test',
            'sku' => 'BTG54F',
        ];

        $this->json('POST', '/api/products', $payload)
            ->assertStatus(200)
            ->assertJson(['id' => 1, 'product_name' => 'Computer', 'product_discription' => 'This is For test','sku' => 'BTG54F']);
    }

    public function testsProductUpdatedCorrectly()
    {
        $product = factory(Product::class)->create([
            'product_name' => 'Computer Test',
            'product_discription' => 'testing',
            'sku' => 'HJu125'
        ]);

        $payload = [
            'product_name' => 'Computer',
            'product_discription' => 'This is For test',
            'sku' => 'BTG54F'
        ];

        $response = $this->json('PUT', '/api/products/' . $product->id, $payload)
            ->assertStatus(200)
            ->assertJson([ 
                'id' => 1, 
                'product_name' => 'Computer', 
                'product_discription' => 'This is For test',
                'sku' =>  'BTG54F'
            ]);
    }

    public function testsProductsDeletedCorrectly()
    {

        $products = factory(Product::class)->create([
            'product_name' => 'Computer',
            'product_discription' => 'This is For test',
            'sku' => 'BTG54F',
        ]);

        $this->json('DELETE', '/api/products/' . $products->id, [])
            ->assertStatus(204);
    }

    public function testProductsListedCorrectly()
    {
        factory(Product::class)->create([
            'product_name' => 'Computer',
            'product_discription' => 'This is For test',
            'sku' => 'BTG54F',
        ]);

        factory(Product::class)->create([
            'product_name' => 'Computer Test',
            'product_discription' => 'testing',
            'sku' => 'HJu125'
        ]);

        $response = $this->json('GET', '/api/products', [])
            ->assertStatus(200)
            ->assertJson([
                [ 'product_name' => 'Computer', 'product_discription' => 'This is For test','sku' => 'BTG54F'],
                [ 'product_name' => 'Computer Test', 'product_discription' => 'testing','sku' => 'HJu125']
            ])
            ->assertJsonStructure([
                '*' => ['id', 'product_name', 'product_discription','sku', 'created_at', 'updated_at'],
            ]);
    }
}
