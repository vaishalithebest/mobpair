<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::truncate();

        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 50; $i++) {
            Product::create([
                'product_name' => $faker->sentence,
                'product_discription' => $faker->paragraph,
                'sku' => $faker->sentence,
            ]);
        }
    }
}
