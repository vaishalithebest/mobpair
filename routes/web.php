<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('productslist', 'CheckapiController@getData');
Route::get('productslist/{id}', 'CheckapiController@getData');
Route::post('productsadd', 'CheckapiController@storeData');
Route::put('productsupdate/{id}', 'CheckapiController@updateData');
Route::delete('products/{id}', 'CheckapiController@deleteData');
